#ifndef _PEFFECT_H_
#define _PEFFECT_H_

#include "Vector3.h"
#include "Particle.h"
#include "Matrix4.h"
#include "Geode.h"

#include <vector>


class ParticleEffect : 
    public Geode
{
public:
	ParticleEffect();

	Matrix4 model2world;

	std::vector<Particle*> particles;

	int num_particles;

	Vector3 position;
	Vector3 direction;
	Vector3 force;

	GLuint texture;

	int milliseconds_since_last_emit;
	int total_delta; // milliseconds

	int emit_rate; // particles per second
	float max_vel;
	float randomness;

    void update();
    void update(double);
    void render();
	void render(double);
};

#endif

