#pragma once

#include <cmath>

struct Vector3f
{
    float x;
    float y;
    float z;

    Vector3f() : Vector3f(0, 0, 0) {}

    Vector3f(float x, float y, float z) : x(x), y(y), z(z)
    {
    }

    void set(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    void normalize()
    {
        float l = length();
        x = x / l;
        y = y / l;
        z = z / l;
    }

    float length_sq()
    {
        return x * x + y * y + z * z;
    }

    float length()
    {
        return sqrt(length_sq());
    }
};
