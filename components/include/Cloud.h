#ifndef _CLOUD_H_
#define _CLOUD_H_

#include "Vector3.h"
#include "Particle.h"
#include "Matrix4.h"

#include <vector>


class Cloud {

public:
	Cloud();

	Matrix4 model2world;

	std::vector<Particle*> particles;

	int num_particles;

	Vector3 position;
	Vector3 direction;
	Vector3 force;

	GLuint texture;
	GLuint LoadTexture(const char * filename, int width, int height);

	int pos;
	int milliseconds_since_last_emit;
	int total_delta; // milliseconds

	int emit_rate; // particles per second
	float max_vel;
	float randomness;

	void update(double);
	void render(double);
};

#endif
