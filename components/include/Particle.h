#ifndef _PARTICLE_H_
#define _PARTICLE_H_

#include "Vector3.h"

class Particle {

public:
	Particle();

	Vector3 position;
	Vector3 velocity;
	Vector3 color;
	//Vector3 rotation_force;

	bool alive;
	
	float age;		// How long the particle has lived
	float lifetime; // How long the particle will be rendered
};

#endif