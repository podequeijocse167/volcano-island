#ifndef _MAIN_H_
#define _MAIN_H_

#include "GLee.h"
#include "Camera.h"
#include "World.h"
#include "Shader.h"

enum tScene
{
    master_scene = 0x01,
    bryan_scene = 0x02,
    daniel_scene = 0x04,
    matheus_scene = 0x08,
};

namespace Globals
{
    extern Camera* camera;
    extern World* world;
    extern Object **curr_obj;
    extern BOOLEAN draw_bounding_sphere;
    extern BOOLEAN control_light;
    extern BOOLEAN per_pixel_lighting;
    extern BOOLEAN culling;
    extern BOOLEAN geometry_debug;
    extern BOOLEAN environment_mapping;
    extern BOOLEAN bump_mapping;
    extern BOOLEAN enable_light;
    extern BOOLEAN shadow_volume;
    extern BOOLEAN render_shadow_volume;
    extern BOOLEAN render_z_buffer;
    extern BOOLEAN render_shadowed_scene;
    extern BOOLEAN render_ambient;
    extern double FOV;
    extern double near_plane;
    extern double far_plane;
    extern double aspect;
    extern tScene scene;
    extern GLuint vertexShaderObject, fragmentShaderObject;
};

#endif