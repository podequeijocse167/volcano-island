#pragma once

#include "Geode.h"

class LSystem :
    public Geode
{
public:
    LSystem();
    ~LSystem();


    UINT32 depth;
    void render();
    void draw_system();
    void update();
    void expand(float num);
};

