#pragma once

struct Vector2f
{
public:
    float x;
    float y;

    Vector2f() : Vector2f(0, 0) {}

    Vector2f(float x, float y) : x(x), y(y)
    {
    }
};

