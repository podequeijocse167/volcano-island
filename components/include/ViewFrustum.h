#pragma once

#include "Node.h"

class Plane
{
public:
    Plane();
    Plane(const Vector4& normal, const Vector4& center);
    Vector4 normal;
    Vector4 center;
};

class ViewFrustum
{
public:
    static double near_plane_height, near_plane_width;
private:
    Plane view_frustum[6];
public:
    ViewFrustum();
    virtual ~ViewFrustum();

    void calculate_frustum();
    BOOLEAN is_in_view_frostum(const Node& object);
};

