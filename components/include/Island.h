#pragma once

#include "ObjectModel.h"
#include "Texture.h"

class Island :
    public ObjectModel
{
protected:
    Texture *texture;
public:
    Island();
    ~Island();

    void render();
};