/* Environment Mapping textures */
static const char cube_map_front[] = { "resources/front.ppm" };
static const char cube_map_back[] = { "resources/back.ppm" };
static const char cube_map_left[] = { "resources/left.ppm" };
static const char cube_map_right[] = { "resources/right.ppm" };
static const char cube_map_top[] = { "resources/top.ppm" };
static const char cube_map_bottom[] = { "resources/bottom.ppm" };

/* Shaders */
static const char ReflectionVertShader[] = { "resources/shader/reflection.vert" };
static const char ReflectionFragShader[] = { "resources/shader/reflection.frag" };
static const char EnvMapVertShader[] = { "resources/shader/envmap.vert" };
static const char EnvMapFragShader[] = { "resources/shader/envmap.frag" };
static const char diffuse_vertex_shader[] = { "resources/shader/diffuse_shading.vert" };
static const char diffuse_frag_shader[] = { "resources/shader/diffuse_shading.frag" };
static const char skybox_vertex_shader[] = { "resources/shader/skybox.vert" };
static const char skybox_frag_shader[] = { "resources/shader/skybox.frag" };
static const char texture2d_vertex_shader[] = { "resources/shader/texture2D.vert" };
static const char texture2d_frag_shader[] = { "resources/shader/texture2D.frag" };
static const char vertex_shader[] = { "resources/shader/shader.vert" };
static const char frag_shader[] = { "resources/shader/shader.frag" };
static const char SillhouetteVertShader[] = { "resources/shader/silhouette.vert" };
static const char SillhouetteGeoShader[] = { "resources/shader/silhouette.geo" };
static const char SillhouetteFragShader[] = { "resources/shader/silhouette.frag" };
static const char LightingVertShader[] = { "resources/shader/lighting.vert" };
static const char LightingFragShader[] = { "resources/shader/lighting.frag" };
static const char NullVertShader[] = { "resources/shader/null_technique.vert" };
static const char NullFragShader[] = { "resources/shader/null_technique.frag" };
static const char ShadowVolumeVertShader[] = { "resources/shader/shadow_volume.vert" };
static const char ShadowVolumeGeoShader[] = { "resources/shader/shadow_volume.geo" };
static const char ShadowVolumeFragShader[] = { "resources/shader/shadow_volume.frag" };
static const char BumpVertShader[] = { "resources/shader/bump.vert" };
static const char BumpFragShader[] = { "resources/shader/bump.frag" };

/* Skybox */

static const char skybox_front[] = { "resources/skybox_front.bmp"  };
static const char skybox_back[] = { "resources/skybox_back.bmp" };
static const char skybox_left[] = { "resources/skybox_left.bmp" };
static const char skybox_right[] = { "resources/skybox_right.bmp" };
static const char skybox_top[] = { "resources/skybox_top.bmp" };
static const char skybox_bottom[] = { "resources/skybox_bottom.bmp" };

/* Models */
static const char bunny_model[] = { "resources/models/bunny.obj" };
static const char island_model[] = { "resources/models/island_final.obj" };
static const char island_texture[] = { "resources/tex/island1_b.bmp" };
static const char island_normal_texture[] = { "resources/tex/island1_normal.bmp" };

/* Particles */
static const char smoke_texture[] = { "resources/tex/smoke.png" };
static const char cloud_texture[] = { "resources/tex/smoke2.png" };

/* Water */
static const char ocean_texture[] = { "resources/tex/ocean_texture.png" };