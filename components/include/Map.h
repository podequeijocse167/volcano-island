#pragma once

#include "Vector3.h"
#include "Matrix4.h"
#include "Node.h"

class Map :
	public Node
{

public:

	Map();

	Matrix4 model2world;

	// Points for two connected Bezier curves (p2 and p4 must form straight line with p3);
	Vector3 p0;
	Vector3 p1;
	Vector3 p2;
	Vector3 p3;
	Vector3 p4;
	Vector3 p5;
	Vector3 p6;

	double verts;

	void draw();



};