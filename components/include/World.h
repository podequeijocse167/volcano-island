#pragma once

#include "Vector4.h"
#include "Geode.h"
#include "Group.h"
#include "Switch.h"
#include "SpotLight.h"
#include "Shader.h"
#include "ViewFrustum.h"
#include "Lamp.h"
#include "Texture.h"
#include "LSystem.h"

#include <vector>

class World :
    Group
{
protected:
    Switch *m_switch_model;
    SpotLight *spot_light;
    Shader *reflection;
    Shader *m_silhouette_shader;
    Shader *m_null_shader;
    Shader *m_lighting_shader;
    Shader *m_shadow_volume_shader;
    Shader *m_bump_shader;
    Shader *m_reflection_shader;
    Shader *m_env_map_shader;
    Light *m_point_light;
    Texture *m_cube_map_texture;
    Lamp *m_sun;
public:
    Texture *m_island_normal_texture;
    Texture *m_island_diffuse_texture;
    ViewFrustum *m_view_frustum;
    static BOOLEAN rendering_shadow_volumes;
    static BOOLEAN rendering_ambient;
    LSystem *m_l_system;
private:
    void make_scene_1(UINT32 mask);
    void make_scene_2(UINT32 mask);
public:
    World();
    ~World();

    void init_spot_light();
    void change_scene();
    Object *get_curr_object();
    Node *get_model();

    void draw(const Matrix4& C);
    void render_scene_into_depth(const Matrix4& C);
    void render_shadow_vol_into_stencil(const Matrix4& C);
    void render_shadowed_scene(const Matrix4& C);
    void render_ambient_light(const Matrix4& C);
    void update();
};