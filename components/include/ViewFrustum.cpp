#include "ViewFrustum.h"
#include "main.h"
#include "Utility.h"

double ViewFrustum::near_plane_height, ViewFrustum::near_plane_width;

Plane::Plane() {}

Plane::Plane(const Vector4& normal, const Vector4& center)
{
    this->normal = normal;
    this->center = center;
    this->normal.normalize();
}

ViewFrustum::ViewFrustum()
{
}

ViewFrustum::~ViewFrustum()
{
}

void ViewFrustum::calculate_frustum()
{
    double dx, dy, x, y, z, FOV_w;
    Matrix4 rotX, rotY;
    Vector4 normal;

    near_plane_height = 2.0 * Globals::near_plane * tan(degree2rad(Globals::FOV / 2.0));
    near_plane_width = Globals::aspect * near_plane_height;
    z = -(Globals::near_plane + Globals::far_plane) / 2.0;
    FOV_w = 2 * rad2degree(atan2(near_plane_width / 2.0, Globals::near_plane));
    dy = tan(degree2rad(Globals::FOV / 2.0)) * (-z - Globals::near_plane);
    dx = tan(degree2rad(FOV_w / 2.0)) * (-z - Globals::near_plane);

    /* Near plane */
    view_frustum[0] = Plane(Vector4(0, 0, 1), Vector4(0, 0, -Globals::near_plane));
    /* Far plane */
    view_frustum[1] = Plane(Vector4(0, 0, -1), Vector4(0, 0, -Globals::far_plane));
    /* Right plane */
    rotY.makeRotateY(-FOV_w / 2.0);
    normal.set(1, 0, 0, 0);
    normal = rotY * normal;
    y = 0;
    x = near_plane_width / 2.0 + dx;
    view_frustum[2] = Plane(normal, Vector4(x, y, z));
    /* Left plane */
    rotY.makeRotateY(FOV_w / 2.0);
    normal.set(-1, 0, 0, 0);
    normal = rotY * normal;
    y = 0;
    x = -x;
    view_frustum[3] = Plane(normal, Vector4(x, y, z));
    /* Up plane */
    y = near_plane_height / 2.0 + dy;
    normal.set(0, 1, 0, 0);
    rotX.makeRotateX(Globals::FOV / 2.0);
    normal = rotX * normal;
    view_frustum[4] = Plane(normal, Vector4(0, y, z));
    /* Down plane */
    y = -y;
    normal.set(0, -1, 0, 0);
    rotX.makeRotateX(-Globals::FOV / 2.0);
    normal = rotX * normal;
    view_frustum[5] = Plane(Vector4(normal), Vector4(0, y, z));
}

BOOLEAN ViewFrustum::is_in_view_frostum(const Node& object)
{
    static Vector4 center;
    double distance;
    double distance_module;
    double radius;
    for (register UINT8 i = 0; i < 6; ++i)
    {
        radius = object.get_bounding_sphere_radius();
        center = object.get_bounding_sphere_center();
        distance = distance2plane(view_frustum[i], center);
        distance_module = distance > 0 ? distance : -distance;
        if (distance > radius)
            return FALSE;
    }
    return TRUE;
}