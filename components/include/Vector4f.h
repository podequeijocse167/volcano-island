#pragma once
struct Vector4f
{
    float x;
    float y;
    float z;
    float w;

    Vector4f() : Vector4f(0, 0, 0, 0) {}

    Vector4f(float x, float y, float z, float w) : x(x), y(y), z(z), w(w)
    {
    }
};
