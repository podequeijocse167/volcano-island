#pragma once

#include "Geode.h"
#include "VerticeList.h"
#include "NormalList.h"
#include "TriangleList.h"
#include "Material.h"
#include "Mesh.h"
#include "Vector3f.h"

#include "GL/glut.h"
#include <vector>

class ObjectModel :
    public Geode
{
protected:
    Material material;
    std::vector<Vector3f> normals, points;
    std::vector<Vector2f> vtexture;
    std::vector<UINT32> faces;
    Vector3f max;
    Vector3f min;
    Mesh mesh;

public:
    ObjectModel();
    ObjectModel(float *vertice, UINT64 n_vertice, float *normal, UINT64 n_normal, UINT32 *triangle, UINT64 n_triangle);
    ObjectModel(const std::string& f_name);
    ~ObjectModel();

    void init(float *vertice, UINT64 n_vertice, float *normal, UINT64 n_normal, UINT32 *triangle, UINT64 n_triangle);
    void init(const std::string& f_name);
    void load_file(std::string filename);
    void generate_buffers();
    virtual void render();
    virtual void reset();

    void calculate_min_max_coords();
};