/*
 Copyright (C) 2006 So Yamaoka

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 */

#ifndef _SHADER_H_
#define _SHADER_H_
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include "glee.h"
#endif

#include "Group.h"

#include <string>
#include <map>
#include <stack>

#define MAX_UNIFORM_LOCATION 128

/*! Handles GLSL shaders.  It can load the code from the file or read straight
 * from the char array. */
class Shader :
    public Group
{
public:
    enum tShaderType
    {
        VERTEX_SHADER = GL_VERTEX_SHADER_ARB,
        FRAGMENT_SHADER = GL_FRAGMENT_SHADER_ARB,
        GEOMETRY_SHADER = GL_GEOMETRY_SHADER_ARB
    };

	GLhandleARB m_shader_prog; // prog id
    std::string vert_name, frag_name, geo_name;
    std::map<std::string, GLint> m_uniform_location_map;
    static BOOLEAN block;
    static std::stack<GLhandleARB> shader_stack;
private:
    UINT32 n_uniform_location;
    BOOLEAN m_enable;
public:
    Shader(const char *vert, const char *frag);
    Shader(const char *vert, const char *frag, const char *geo);
	~Shader();

    void init();
    void add_shader(tShaderType type, const char *file);
    GLint add_uniform_location(std::string name);
    void set_uniform_vec3f(std::string name, GLsizei count, const GLfloat* value);
    void set_uniform_mat3f(std::string name, GLsizei count, BOOLEAN transpose, const GLfloat *value);
    void set_uniform1i(std::string name, GLint value);
    GLint& operator[](std::string name_uniform_location);

	/** bind/unbind this shader. */
	void bind();
	void unbind();
    static void push();
    static void pop();
    void enable() { m_enable = TRUE; }
    void disable() { m_enable = FALSE; }

	/** get the program */
    GLhandleARB getPid();

	/** dump log */
	void printLog(const char* tag = "");

    virtual void draw(const Matrix4& C);

private:
	/** read a shader file
	 programmer is responsible for deleting returned buffer */
	char* read(const char *filename);

	/** create shaders from given vert and frag programs. */
    void setup(tShaderType type, const char *shader);
    char* shader_type2string(tShaderType type);
    BOOLEAN finalize();
};


#endif
