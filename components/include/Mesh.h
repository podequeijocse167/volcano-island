#pragma once

#include "DataType.h"
#include "Vector3f.h"
#include "Vector2f.h"
#include "Utility.h"

#include <assert.h>
#include <string>
#include <vector>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "GL\glut.h"

struct Edge
{
    UINT32 a;
    UINT32 b;

    inline Edge(UINT32 _a, UINT32 _b)
    {
        assert(_a != _b);

        if (_a < _b)
        {
            a = _a;
            b = _b;
        }
        else
        {
            a = _b;
            b = _a;
        }
    }

    void print()
    {
        printf("Edge %d %d\n", a, b);
    }
};

struct Face
{
    UINT32 index[3];

    inline UINT32 get_opposite_index(const Edge& e) const
    {
        for (UINT32 i = 0; i < ARRAY_SIZE_IN_ELEMENTS(index); i++)
        {
            UINT32 Index = index[i];

            if (Index != e.a && Index != e.b)
            {
                return Index;
            }
        }

        assert(0);

        return 0;
    }
};

struct Neighbors
{
    UINT32 n1;
    UINT32 n2;

    inline Neighbors()
    {
        n1 = n2 = (UINT32)-1;
    }

    inline void add_neighbor(UINT32 n)
    {
        if (n1 == -1)
        {
            n1 = n;
        }
        else if (n2 == -1)
        {
            n2 = n;
        }
        else
        {
            //assert(0);
        }
    }

    inline UINT32 get_other(UINT32 me) const
    {
        if (n1 == me)
        {
            return n2;
        }
        else if (n2 == me)
        {
            return n1;
        }
        else
        {
           // assert(0);
        }

        return 0;
    }
};

struct CompareEdges
{
    inline bool operator()(const Edge& Edge1, const Edge& Edge2)
    {
        if (Edge1.a < Edge2.a)
        {
            return true;
        }
        else if (Edge1.a == Edge2.a)
        {
            return (Edge1.b < Edge2.b);
        }
        else
        {
            return false;
        }
    }
};

struct CompareVectors
{
    inline bool operator()(const Vector3f& a, const Vector3f& b)
    {
        if (a.x < b.x)
        {
            return true;
        }
        else if (a.x == b.x)
        {
            if (a.y < b.y)
            {
                return true;
            }
            else if (a.y == b.y)
            {
                if (a.z < b.z)
                {
                    return true;
                }
            }
        }

        return false;
    }
};

class Mesh
{
public:
    class Vertex
    {
    public:
        Vector3f m_pos;
        Vector2f m_tex;
        Vector3f m_normal;

        Vertex() {}

        inline Vertex(const Vector3f& pos, const Vector2f& tex, const Vector3f& normal)
        {
            m_pos = pos;
            m_tex = tex;
            m_normal = normal;
        }
    };

    GLint m_withAdjacencies;
private:
    class MeshEntry
    {
    public:
        MeshEntry() {}

        ~MeshEntry() {}

        void init(const std::vector<Vertex>& Vertices,
            const std::vector<unsigned int>& Indices);

        GLuint VB;
        GLuint IB;
        UINT32 NumIndices;
    };

    std::vector<MeshEntry> m_entries;
    std::map<Vector3f, UINT32, CompareVectors> m_posMap;
    std::map<Edge, Neighbors, CompareEdges> m_indexMap;
    std::vector<Face> m_uniqueFaces;
public:
    Mesh();
    Mesh(GLuint with_adj);
    virtual ~Mesh();

    void clear();
    bool load_mesh(const std::string& Filename);
    bool init_from_scene(const aiScene* pScene, const std::string& Filename);
    void init_mesh(UINT32 Index, const aiMesh* paiMesh);
    void init_mesh(Vector3f *vertice, Vector2f *texture, Vector3f *normal, UINT32 n_vertices, Face *faces, UINT32 n_faces);
    void find_adjacencies(Vector3f *vertices, const Face* faces, UINT32 n_faces, std::vector<UINT32>& Indices);
    void render();
};

