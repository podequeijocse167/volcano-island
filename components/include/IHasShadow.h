#pragma once

#include <list>

class IHasShadow
{
public:
    static std::list<IHasShadow*> list;

    IHasShadow();
    ~IHasShadow();

    virtual void update() = 0;
};

