#include "Skybox.h"
#include "MatrixTransform.h"
#include "Texture.h"
#include "Utility.h"
#include "Shader.h"
#include "Box.h"
#include "Resources.h"

Skybox::Skybox()
{
    MatrixTransform *transform;
	Material *material;
    Box *box;
    Matrix4 tmp;

    box = new Box();
	material = new Material();
	material->set_property(Material::AMBIENT, 0.0f, 0.0f, 0.0f);
	material->set_property(Material::DIFFUSE, 0.0f, 0.0f, 0.0f);

    tmp.makeScale(100);
    tmp *= Matrix4(Matrix4::tTranslate, 0, 0, 0);
    transform = new MatrixTransform(tmp.clone());
	this->add_child(transform);
    transform->add_child(material);
	material->add_child(new Box());

    /* Skybox is not affect at AMBIENT rendering step */
    box->set_get_shadow(FALSE);
}

Skybox::~Skybox()
{
}

void Skybox::draw(const Matrix4& C)
{
    if (World::rendering_ambient || World::rendering_shadow_volumes)
        return;
    glDepthMask(GL_FALSE);
    Group::draw(C);
    glDepthMask(GL_TRUE);

    GL_DEBUG("Skybox::draw");
}
