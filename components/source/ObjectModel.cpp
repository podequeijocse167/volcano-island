#include "ObjectModel.h"
#include "Config.h"
#include "Utility.h"
#include "main.h"
#include "Window.h"
#include "ModelLoader.h"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

using namespace std;

static UINT32 count_lines(const char *f_name);

const int MAX_CHARS_PER_LINE = 512;
const int MAX_TOKENS_PER_LINE = 20;
const char* const DELIMITER = " ";

ObjectModel::ObjectModel()
{
}

ObjectModel::ObjectModel(float *vertice, UINT64 n_vertice, float *normal,
    UINT64 n_normal, UINT32 *triangle, UINT64 n_triangle) : ObjectModel()
{
    init(vertice, n_vertice, normal, n_normal, triangle, n_triangle);
}

ObjectModel::ObjectModel(const std::string& f_name) : ObjectModel()
{
    init(f_name);
}

void ObjectModel::init(float *vertice, UINT64 n_vertice, float *normal,
    UINT64 n_normal, UINT32 *triangle, UINT64 n_triangle)
{
    mesh.init_mesh((Vector3f*)vertice, nullptr, (Vector3f*)normal, n_vertice, (Face*)triangle, n_triangle);
}

void ObjectModel::init(const std::string& f_name)
{
    //mesh.load_mesh(f_name);
    this->load_file(f_name);
    generate_buffers();
    mesh.init_mesh(&points[0], &vtexture[0], &normals[0], points.size(), (Face*)&faces[0], faces.size() / 3);
    reset();
}

ObjectModel::~ObjectModel()
{
}

void ObjectModel::generate_buffers()
{
    std::map<UINT32, UINT32> point_map;
    std::vector<Vector3f> p;
    std::vector<Vector3f> n;
    std::vector<Vector2f> t;
    std::vector<UINT32> f;
    UINT32 index;
    
    for (UINT32 i = 0; i < faces.size() / 3; i++)
    {
        UINT32 vi = faces[i * 3];
        UINT32 vti = faces[i * 3 + 1];
        UINT32 ni = faces[i * 3 + 2];

        /*if (point_map.find(vi) == point_map.end())
        {
            point_map[vi] = i;
            index = i;
        }
        else
        {
            index = point_map[vi];
        }*/
        
        p.push_back(Vector3f(points[vi].x, points[vi].y, points[vi].z));
        n.push_back(Vector3f(-1 * normals[ni].x, -1 * normals[ni].y, -1 * normals[ni].z));
        t.push_back(Vector2f(vtexture[vti].x, vtexture[vti].y));
        f.push_back(i);
    }

    points = p;
    normals = n;
    vtexture = t;
    faces = f;
}

void ObjectModel::render()
{
    material.render();
    mesh.render();
    material.set_default_material();
}

void ObjectModel::reset()
{
    // Center model vertices
    calculate_min_max_coords();

    double lx = max.x - min.x;
    double ly = max.y - min.y;
    double zPlaneSize = 2 * 20 * std::tan(30 * 3.1415 / 180);
    double cover = 1.2;
    double scaleX = (zPlaneSize * cover) / lx;
    double scaleY = (zPlaneSize * cover) / ly;
    double s0 = (scaleX > scaleY) ? scaleY : scaleX;
    Vector3f model_center(min.x + (max.x - min.x) / 2, min.y + (max.y - min.y) / 2, min.z + (max.z - min.z) / 2);

    model2world = Matrix4(Matrix4::tTranslate, -s0 * model_center.x, -s0 * model_center.y, -s0 * model_center.z);
    model2world = model2world * Matrix4(Matrix4::tScale, s0, s0, s0);

    Object::updateMatrix();
}

void ObjectModel::load_file(std::string filename)
{
    std::ifstream fin;

	model2world.identity();

	max.set(0, 0, 0);
	min.set(0, 0, 0);

	// create a file-reading object
	fin.open(filename); // open a file
    if (!fin.good())
    {
        std::cout << "File problem!" << std::endl;
        return;
    }

	// read each line of the file
	while (!fin.eof())
	{
		// read an entire line into memory
		char buf[MAX_CHARS_PER_LINE];
		fin.getline(buf, MAX_CHARS_PER_LINE);

		// parse the line into blank-delimited tokens
		int n = 0; // a for-loop index

		// array to store memory addresses of the tokens in buf
		const char* token[MAX_TOKENS_PER_LINE] = {}; // initialize to 0

		// If line start with '#' ignore it
		if (buf[0] == '#') {
			continue;
		}

		// Else parse the line (subdivide line in 'words')
		token[0] = std::strtok(buf, DELIMITER); // first token
		if (token[0]) { // (token[0] = zero if line is blank)

			for (n = 1; n < MAX_TOKENS_PER_LINE; n++) {
				token[n] = std::strtok(0, DELIMITER); // subsequent tokens
				if (!token[n]) break; // no more tokens
			}

			if (std::strcmp(token[0], "vn") == 0) { // Vertex normal
				Vector3f n(std::atof(token[1]), std::atof(token[2]), std::atof(token[3]));
				n.normalize();
				normals.push_back(n);
			}

			if (std::strcmp(token[0], "v") == 0) { // Vertex
				Vector3f p(std::atof(token[1]), std::atof(token[2]), std::atof(token[3]));
				points.push_back(p);
			}

			if (std::strcmp(token[0], "vt") == 0) { // Vertex Texture
				Vector2f p(std::atof(token[1]), std::atof(token[2]));
				vtexture.push_back(p);
			}

			if (std::strcmp(token[0], "f") == 0) { // Face
				for (int i = 1; i < 4; i++) {
					std::string s = token[i];
					char *b = new char[s.length() + 1];
					strcpy(b, s.c_str());
					
					const char* v_index_char = std::strtok(b, "/"); // first token
					const char* vt_index_char = std::strtok(0, "/");
					const char* n_index_char = std::strtok(0, "/");

					std::stringstream strValue1;
					std::stringstream strValue2;
					std::stringstream strValue3;

					unsigned long int v_index;
					unsigned long int vt_index;
					unsigned long int n_index;

					strValue1 << v_index_char;					
					strValue1 >> v_index;

					strValue2 << vt_index_char;
					strValue2 >> vt_index;

					strValue3 << n_index_char;
					strValue3 >> n_index;

					faces.push_back(v_index - 1);
					faces.push_back(vt_index - 1);
					faces.push_back(n_index - 1);

					delete[] b;

				}			
			}
		}		
	}

    reset();
}

void ObjectModel::calculate_min_max_coords()
{
    max.set(points[0].x, points[0].y, points[0].z);
    min.set(points[0].x, points[0].y, points[0].z);

    for (std::vector<Vector3>::size_type i = 0; i != points.size(); i++)
    {
        if (points[i].x > max.x) max.x = points[i].x;
        if (points[i].y > max.y) max.y = points[i].y;
        if (points[i].z > max.z) max.z = points[i].z;

        if (points[i].x < min.x) min.x = points[i].x;
        if (points[i].y < min.y) min.y = points[i].y;
        if (points[i].z < min.z) min.z = points[i].z;
    }
}

