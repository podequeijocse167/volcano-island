#include "main.h"
#include "World.h"
#include "Utility.h"
#include "MatrixTransform.h"
#include "MotionTransform.h"
#include "IUpdatable.h"
#include "Geode.h"
#include "Wall.h"
#include "Dragon.h"
#include "SpotLight.h"
#include "Patch.h"
#include "Skybox.h"
#include "Texture.h"
#include "Bunny.h"
#include "Resources.h"
#include "Island.h"
#include "LSystem.h"
#include "ParticleEffect.h"

#include <chrono>
#include <iostream>

BOOLEAN World::rendering_shadow_volumes = FALSE;
BOOLEAN World::rendering_ambient = FALSE;

World::World()
{
    m_view_frustum = new ViewFrustum();
    m_switch_model = new Switch();
    this->add_child(m_switch_model);

    change_scene();
    m_view_frustum->calculate_frustum();

    m_silhouette_shader = new Shader(SillhouetteVertShader, SillhouetteFragShader, SillhouetteGeoShader);
    m_silhouette_shader->add_uniform_location("gLightPos");
    m_null_shader = new Shader(NullVertShader, NullFragShader);
    m_shadow_volume_shader = new Shader(ShadowVolumeVertShader, ShadowVolumeFragShader, ShadowVolumeGeoShader);
    m_shadow_volume_shader->add_uniform_location("gLightPos");
    m_lighting_shader = new Shader(LightingVertShader, LightingFragShader);
    m_lighting_shader->add_uniform_location("gNumPointLights");
    m_lighting_shader->add_uniform_location("gNumSpotLights");
    m_lighting_shader->add_uniform_location("gDirectionalLight");
    m_lighting_shader->add_uniform_location("gPointLights");
    m_lighting_shader->add_uniform_location("gSpotLights");
    m_lighting_shader->add_uniform_location("gColorMap");
    m_lighting_shader->add_uniform_location("gEyeWorldPos");
    m_lighting_shader->add_uniform_location("gMatSpecularIntensity");
    m_lighting_shader->add_uniform_location("gSpecularPower");
    m_bump_shader = new Shader(BumpVertShader, BumpFragShader);
    m_bump_shader->add_uniform_location("diffuseTexture");
    m_bump_shader->add_uniform_location("normalTexture");
    m_reflection_shader = new Shader(ReflectionVertShader, ReflectionFragShader);
    m_reflection_shader->add_uniform_location("CameraMatrix");
    m_env_map_shader = new Shader(EnvMapVertShader, EnvMapFragShader);
    
    /* Texture */
    m_cube_map_texture = new Texture(Texture::tTextureType::TEXTURE_CUBE_MAP);
    m_cube_map_texture->make_cube_map(skybox_back, skybox_front, skybox_top, skybox_bottom, skybox_left, skybox_right);
    m_island_diffuse_texture = new Texture(Texture::tTextureType::TEXTURE_2D, island_texture);
    m_island_normal_texture = new Texture(Texture::tTextureType::TEXTURE_2D, island_normal_texture);

    GL_DEBUG("World - getting uniform location");    

    make_scene_1(0x01);
    make_scene_2(0x02);
    m_switch_model->set_mask(0x01);
}

World::~World() {
    delete m_view_frustum;
}

void World::make_scene_1(UINT32 mask)
{
    MatrixTransform *matrix_transform;
    Matrix4 transform;
    Material *material;
    Bunny *bunny;
    Island *island;
    Group *scene = new Group();

    /* Skybox */
    scene->add_child(m_cube_map_texture);
    m_cube_map_texture->add_child(new Skybox());

    /* Water */
    transform = Matrix4(Matrix4::tTranslate, 0, 0, 0) * Matrix4(Matrix4::tRotate, -90, 1, 0, 0);
    material = new Material();
    material->set_property(Material::DIFFUSE, 0.6, 0.6, 1.0);
    matrix_transform = new MatrixTransform(transform.clone());
    m_cube_map_texture->add_child(material);
    material->add_child(matrix_transform);
    matrix_transform->add_child(m_reflection_shader);
    m_reflection_shader->add_child(new Wall(200, 200));

    /* Island */
    if (1)
    {
        island = new Island();
        matrix_transform = new MatrixTransform(new Matrix4(Matrix4::tTranslate, Vector3(0, 0, 0)));
        scene->add_child(matrix_transform);
        matrix_transform->add_child(m_lighting_shader);
        m_lighting_shader->add_child(island);
        matrix_transform = new MatrixTransform(new Matrix4(Matrix4::tScale, 1, -1, 1));
        //matrix_transform->add_child(island);
        scene->add_child(matrix_transform);
    }

    /* Particles */
    if (1)
    {
        scene->add_child(new ParticleEffect());
        //scene->add_child(new Clould());
    }

    /* L-System */
    if (1)
    {
        m_l_system = new LSystem();
        transform = Matrix4(Matrix4::tTranslate, 0, 2, 0) * Matrix4(Matrix4::tScale, 0.01, 0.01, 0.01);
        matrix_transform = new MatrixTransform(new Matrix4(Matrix4::tTranslate, Vector3(0, 2, 0)));
        scene->add_child(matrix_transform);
        matrix_transform->add_child(m_l_system);
    }

    /* Bunny */
    if (1)
    {
        bunny = new Bunny();
        material = new Material();
        material->set_property(Material::AMBIENT, 0.1, 0.4, 1.0);
        material->set_property(Material::DIFFUSE, 0.7, 0.5, 5.0);
        material->set_property(Material::SPECULAR, 0.2, 0.2, 1.0);
        material->set_property(Material::SHININESS, 32);
        matrix_transform = new MatrixTransform(new Matrix4(Matrix4::tTranslate, -5, 1, 0));
        scene->add_child(material);
        material->add_child(matrix_transform);
        matrix_transform->add_child(bunny);
        matrix_transform->add_child(m_silhouette_shader);
        m_silhouette_shader->add_child(bunny);
    }

    /* Lights */
    /* Lamp */
    transform.makeTranslate(2, 6, 0);
    matrix_transform = new MatrixTransform(transform.clone());
    m_sun = new Lamp(GL_LIGHT0, 1);
    m_point_light = m_sun->get_light_pointer();
    scene->add_child(matrix_transform);
    matrix_transform->add_child(m_sun);

    m_switch_model->add_child(scene, mask);
}

void World::make_scene_2(UINT32 mask)
{
    Matrix4 transform;
    Group *scene = new Group();

    transform = Matrix4(Matrix4::tTranslate, 0, 0, 0) * Matrix4(Matrix4::tRotate, 90, 1, 0, 0);
    scene->add_child(new Wall());

    m_switch_model->add_child(scene, mask);
}

void World::change_scene()
{
    Globals::camera->set_center_of_proj(Vector4(0, 5, 10));
    Globals::camera->look_at(Vector4(0, 0, 0));
    Globals::camera->set_up(Vector4(0, 1, 0));

    m_switch_model->set_mask(Globals::scene);
}

Object *World::get_curr_object()
{
    return m_sun;
}

Node *World::get_model()
{
    return m_switch_model->get_first_element();
}

void World::draw(const Matrix4& C)
{
    GL_DEBUG("World::draw - start");

    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);
    glEnable(GL_DEPTH_TEST);
    glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
    glDepthMask(GL_TRUE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    glDepthFunc(GL_LEQUAL);

    GL_DEBUG("World:: - setup");

    m_silhouette_shader->bind();
    m_silhouette_shader->set_uniform_vec3f("gLightPos", 1, m_point_light->get_position_eye_space3f());
    m_silhouette_shader->unbind();

    m_reflection_shader->bind();
    m_reflection_shader->set_uniform_mat3f("CameraMatrix", 1, GL_TRUE, Globals::camera->getMatrix().get_pointer3f());
    m_reflection_shader->unbind();

    m_bump_shader->bind();
    m_bump_shader->set_uniform1i("diffuseTexture", 0);
    m_bump_shader->set_uniform1i("normalTexture", 1);
    m_bump_shader->unbind();

    if (Globals::geometry_debug)
        m_silhouette_shader->enable();
    else
        m_silhouette_shader->disable();

    if (Globals::bump_mapping == TRUE)
        m_bump_shader->enable();
    else
        m_bump_shader->disable();

    GL_DEBUG("World:: - set uniform locations");

    if (Globals::shadow_volume)
    {
        render_scene_into_depth(C);
        glEnable(GL_STENCIL_TEST);
        render_shadow_vol_into_stencil(C);
        render_shadowed_scene(C);
        glDisable(GL_STENCIL_TEST);
        render_ambient_light(C);
    }
    else
    {
        render_scene_into_depth(C);
        render_shadowed_scene(C);
        render_ambient_light(C);
    }
    if (Globals::render_shadowed_scene == FALSE)
        Group::draw(C);

    GL_DEBUG("World::draw - end");
}

void World::render_scene_into_depth(const Matrix4& C)
{
    if (Globals::render_z_buffer == FALSE)
        return;
    glDrawBuffer(GL_NONE);

    m_null_shader->bind();
    
    Group::draw(C);

    m_null_shader->unbind();

    GL_DEBUG("World::render_scene_into_depth");
}

void World::render_shadow_vol_into_stencil(const Matrix4& C)
{
    if (Globals::render_shadow_volume == FALSE)
        return;
    glDepthMask(GL_FALSE); /* Disable writting to z-buffer */
    glEnable(GL_DEPTH_CLAMP_NV); /* Clamp the volume at the infinity */
    glDisable(GL_CULL_FACE);
    if (Globals::geometry_debug)
        glDrawBuffer(GL_BACK);

    // We need the stencil test to be enabled but we want it
    // to succeed always. Only the depth test matters.
    glStencilFunc(GL_ALWAYS, 0, 0xff);

    // Set the stencil test per the depth fail algorithm
    glStencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
    glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);
    
    m_shadow_volume_shader->bind();
    m_shadow_volume_shader->set_uniform_vec3f("gLightPos", 1, m_point_light->get_position_eye_space3f());
    Shader::block = TRUE;

    // Render the occluder 
    GL_DEBUG("World::render_shadow_vol_into_stencil");
    rendering_shadow_volumes = TRUE;
    Group::draw(C);
    rendering_shadow_volumes = FALSE;

    Shader::block = FALSE;
    m_shadow_volume_shader->unbind();
    
    // Restore local stuff
    glDisable(GL_DEPTH_CLAMP_NV);
    glEnable(GL_CULL_FACE);
    GL_DEBUG("World::render_shadow_vol_into_stencil");
}

void World::render_shadowed_scene(const Matrix4& C)
{
    if (Globals::render_shadowed_scene == FALSE)
        return;
    glDrawBuffer(GL_BACK);
    // Draw only if the corresponding stencil value is zero
    glStencilFunc(GL_EQUAL, 0x0, 0xFF);

    // prevent update to the stencil buffer
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

    m_point_light->ambient_intensity = 0.0f;
    m_point_light->diffuse_intensity = 1.0f;
    m_point_light->specular_intensity = 1.0f;

    m_lighting_shader->bind();

    m_point_light->render();

    Group::draw(C);

    m_lighting_shader->unbind();

    GL_DEBUG("World::render_shadowed_scene");
}

void World::render_ambient_light(const Matrix4& C)
{
    if (Globals::render_ambient == FALSE)
        return;
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);

    m_point_light->ambient_intensity = 0.8f;
    m_point_light->diffuse_intensity = 0.0f;
    m_point_light->specular_intensity = 0.0f;

    m_point_light->render();

    rendering_ambient = TRUE;
    Group::draw(C);
    rendering_ambient = FALSE;

    glDisable(GL_BLEND);
    GL_DEBUG("World::render_ambient_light");
}

void World::update()
{
    IUpdatable::call_update();
}