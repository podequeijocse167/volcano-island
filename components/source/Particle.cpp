#include "Particle.h"

Particle::Particle() {
	alive = true;

	position.set(0, 0, 0);
	velocity.set(0, 1, 0);
	color.set(0.5, 0.5, 0.5);

	age = 0;
	lifetime = 50000; // 4 seconds
}
