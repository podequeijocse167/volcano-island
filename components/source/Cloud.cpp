#include "Cloud.h"

#include <GL/glext.h>

#include "Vector3.h"
#include "Matrix4.h"


Cloud::Cloud() {

	//int num_particles = 10;

	pos = 0;

	model2world.identity();

	direction.set(0.012, 0, 0);
	force.set(0, 0, 0);

	max_vel = 0.02;

	milliseconds_since_last_emit = 0;
	total_delta = 0;

	emit_rate = 10;

	Particle* p = new Particle();
	p->velocity.set(direction[0], direction[1], direction[2]);
	particles.push_back(p);
}

static double rn() {

	return 2 * ((double(rand()) / double(RAND_MAX)) - 0.5);


}

void Cloud::update(double delta) {
	double rf = 0.004;
	double rf2 = 0.00004;
	double factor = delta * 0.1;
	Vector3 r1, r2;

	for (int i = 0; i < particles.size(); i++) {

		Particle* p = particles[i];

		if (p->alive == true) {
            r1.set(rf*rn(), rf*rn(), rf*rn());
			r2.set(rf2*rn(), rf2*rn(), rf2*rn());

			p->position = p->position + Vector3(factor*p->velocity[0], factor*p->velocity[0], factor*p->velocity[1]) + r1;
			p->age += delta;

			if (p->age > p->lifetime) {
				p->alive = false;
				num_particles -= 1;

			}
			// UPDATE PARTICLE LIFE AND CHECK IF DEAD
		}
	}

	// Create new particles (TODO ROTATE OLD PARTICLES)

	total_delta = delta + milliseconds_since_last_emit;

	//int seconds = 1000 / delta;
	int milliseconds_between_emits = 1000 / emit_rate;
	int to_emit = total_delta / milliseconds_between_emits;

	rf = 0.5;

	if (to_emit > 0 && num_particles < 50) { // emit at least one particle
		// REUSE DEAD PARTICLES
		for (int i = 0; i < particles.size() && to_emit > 0; i++) {
			Particle* p = particles[i];
			if (p->alive == false) {
				p->alive = true;
				
				p->age = 0;
				r1.set(rf*rn(), rf*rn(), rf*rn());

				Vector3 posI;

				if (pos == 0) posI.set(-40, 0, -50);

				if (pos == 1) posI.set(10, 0, -30);

				if (pos == 2) posI.set(-30, 0, 0);

				if (pos == 3) {
					pos = -1;
					posI.set(-10, 0, -20);
				}
				else {
					pos += 1;
				}

				p->position = posI;
				p->position += r1;
				p->velocity.set(direction[0], direction[1], direction[2]);
				num_particles += 1;
				to_emit -= 1;
				break;
			}
		}

		for (int i = 0; i < to_emit; i++) {
			//std::cout << "CREATING NEW" << std::endl;
			Particle* p = new Particle();
			p->velocity.set(direction[0], direction[1], direction[2]);
			r1.set(rf*rn(), rf*rn(), rf*rn());

			Vector3 posI;

			if (pos == 0) posI.set(-40, 0, -50);

			if (pos == 1) posI.set(10, 0, -30);

			if (pos == 2) posI.set(-30, 0, 0);

			if (pos == 3) {
				pos = -1;
				posI.set(-10, 0, -20);
			}

			pos += 1;

			p->position = posI;

			p->position += r1;
			num_particles += 1;
			particles.push_back(p);
		}
		milliseconds_since_last_emit = 0;

	}
	else {
		if (num_particles >= 50) {
			milliseconds_since_last_emit = 0;
		}
		else {
		    milliseconds_since_last_emit += delta;
		}
	}

}


void Cloud::render(double cam_dist) {

	glDepthMask(GL_FALSE);

	//glMultMatrixf(model2world.getGLValues());

	// Draw as point

	glBindTexture(GL_TEXTURE_2D, texture);

	float quadratic[] = { 0.0f, 0.0f, 0.01f };

	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE_ARB);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_POINT_SPRITE);

	//glPointParameter
	//glPointParameterfvARB(GL_POINT_DISTANCE_ATTENUATION_ARB, quadratic);
	float maxSize = 0.0f;
	glGetFloatv(GL_POINT_SIZE_MAX_ARB, &maxSize);
	//glSprite
	glPointSize(15 * cam_dist*cam_dist);
	//glPointParameterfARB(GL_POINT_SIZE_MAX_ARB, maxSize);
	//glPointParameterfARB(GL_POINT_SIZE_MIN_ARB, 1.0f);
	glTexEnvf(GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB, GL_TRUE);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_POINT_SPRITE_ARB);

	glBegin(GL_POINTS);


	for (int i = 0; i < particles.size(); i++) {

		Particle* p = particles[i];

		if (p->alive == true) {

			glColor3d(p->color[0], p->color[1], p->color[2]);
			glVertex3d(p->position[0], p->position[1], p->position[2]);

		}
	}

	glEnd();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_POINT_SPRITE_ARB);
	glDepthMask(GL_TRUE);
}
