#include "ParticleEffect.h"
#include "Resources.h"
#include "Shader.h"
#include "Config.h"

#include "SOIL/SOIL.h"
#include <GL/glext.h>

#include "Vector3.h"
#include "Matrix4.h"

ParticleEffect::ParticleEffect() {
    num_particles = 10;

	model2world.identity();

	direction.set(0, 0.012, 0);
	force.set(0.00001, -0.000005, 0);
	max_vel = 0.02;
	milliseconds_since_last_emit = 0;
	total_delta = 0;
	emit_rate = 20;

    texture = SOIL_load_OGL_texture
        (
        smoke_texture,
        SOIL_LOAD_AUTO,
        SOIL_CREATE_NEW_ID,
        SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
        );


    for (int i = 0; i < num_particles; i++) {
	Particle* p = new Particle();
	p->velocity.set(direction[0], direction[1], direction[2]);
    particles.push_back(p);
    }
    model2world.makeTranslate(1.5, 3, -1);
}

static double rn() {
	return 2 * ((double(rand()) / double(RAND_MAX)) - 0.5);
}

void ParticleEffect::update()
{
    ParticleEffect::update(dt * 100);
}

void ParticleEffect::update(double delta) {

	// Update existing particles
	double rf = 0.004;
	double rf2 = 0.00004;
	double factor = delta * 0.1;
	Vector3 r1, r2;

	for (UINT32 i = 0; i < particles.size(); i++) {
		Particle* p = particles[i];

		if (p->alive == true) {
			r1.set(rf*rn(), rf*rn(), rf*rn());
			r2.set(rf2*rn(), rf2*rn(), rf2*rn());

			p->position = p->position + Vector3(factor*p->velocity[0], factor*p->velocity[1], factor*p->velocity[2]) + r1;

			if (p->velocity.length() < max_vel) {
				p->velocity = p->velocity + Vector3(factor*force[0], factor*force[1], factor*force[2]) + r2;
			}


			p->age += delta;

			if (p->age > p->lifetime) {
				p->alive = false;

			}
			// UPDATE PARTICLE LIFE AND CHECK IF DEAD
		}
	}

	// Create new particles (TODO ROTATE OLD PARTICLES)

	total_delta = delta + milliseconds_since_last_emit;

	//int seconds = 1000 / delta;
	int milliseconds_between_emits = 1000 / emit_rate;
	int to_emit = total_delta / milliseconds_between_emits;	

	if (to_emit > 0) { // emit at least one particle
		// REUSE DEAD PARTICLES
		for (UINT32 i = 0; i < particles.size() && to_emit > 0; i++) {
			Particle* p = particles[i];
			if (p->alive == false) {
				p->alive = true;

				p->age = 0;
				p->position.set(0, 0, 0);
				p->velocity.set(direction[0], direction[1], direction[2]);
				
				to_emit -= 1;
				break;
			}
		}

		for (int i = 0; i < to_emit; i++) {
			Particle* p = new Particle();
			p->velocity.set(direction[0], direction[1], direction[2]);
			particles.push_back(p);
		}
		milliseconds_since_last_emit = 0;
	}
	else {
		milliseconds_since_last_emit += delta;
	}
}

void ParticleEffect::render()
{
    render(2);
}

void ParticleEffect::render(double cam_dist)
{
    Matrix4 transform = model2world;
    float quadratic[] = { 0.0f, 0.0f, 0.01f };
    float maxSize = 0.0f;
    Shader::push();

    glMatrixMode(GL_MODELVIEW);
    glMultMatrixf(transform.transpose().get_pointer3f());

	// Draw as point
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE_ARB);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_POINT_SPRITE);
    glGetFloatv(GL_POINT_SIZE_MAX_ARB, &maxSize);
	glPointSize(10 * cam_dist*cam_dist);
	glTexEnvf(GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB, GL_TRUE);

	glEnable(GL_POINT_SPRITE_ARB);

	glBegin(GL_POINTS);

    for (UINT32 i = 0; i < particles.size(); i++)
    {
        Particle* p = particles[i];

        if (p->alive == true)
        {
            glColor3d(p->color[0], p->color[1], p->color[2]);
            glVertex3d(p->position[0], p->position[1], p->position[2]);
        }
    }

    glEnd();
    glDisable(GL_POINT_SPRITE_ARB);
    glDisable(GL_POINT_SPRITE);
    glDisable(GL_VERTEX_PROGRAM_POINT_SIZE_ARB);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_POINT_SPRITE_ARB);
	glDepthMask(GL_TRUE);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);
    Shader::pop();
}