#include "Window.h"
#include "Cube.h"
#include "Matrix4.h"
#include "main.h"
#include "Config.h"
#include "Utility.h"

#include <iostream>

using namespace std;

int Window::width = 1024;   // set window width in pixels here
int Window::height = 1024;   // set window height in pixels here

//----------------------------------------------------------------------------
// Callback method called when system is idle.
void Window::idleCallback()
{
    Globals::world->update();
    displayCallback();
}

//----------------------------------------------------------------------------
// Callback method called by GLUT when graphics window is resized by the user
void Window::reshapeCallback(int w, int h)
{
    cerr << "Window::reshapeCallback called" << endl;
    width = w;
    height = h;
    Globals::aspect = (double)width / (double)height;
    glViewport(0, 0, w, h);  // set new view port size

	// Set Projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(Globals::FOV, double(width) / (double)height, Globals::near_plane, Globals::far_plane); // set perspective projection viewing frustum
	displayCallback();

    Globals::world->m_view_frustum->calculate_frustum();
}

//----------------------------------------------------------------------------
// Callback method called by GLUT when window readraw is necessary or when glutPostRedisplay() was called.
void Window::displayCallback()
{
	// Clear color and depth buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  

	// TODO CHANGE IDENTITY TO INVERSE CAMERA MATRIX
    Globals::world->draw(Globals::camera->getMatrix());

	// If DEBUG mode, draw axes
#if (DEBUG_GRAPHICS == TRUE) /* Draw the axis x, y, z */
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glColor3f(1.0, 1.0, 1.0);
    draw_line(-1000, 0, 0, 1000, 0, 0);
    draw_line(0, -1000, 0, 0, 1000, 0);
    draw_line(0, 0, -1000, 0, 0, 1000);
    glPopMatrix();
#endif

    glFlush();
    glutSwapBuffers();
}
