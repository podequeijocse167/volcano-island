#include "main.h"
#include "Window.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "Matrix4.h"
#include "Utility.h"
#include "Texture.h"
#include "Common.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

namespace Globals
{
    Camera* camera;
    World* world;
    Object** curr_obj;

    BOOLEAN draw_bounding_sphere = FALSE;
    BOOLEAN control_light = FALSE;
    BOOLEAN per_pixel_lighting = FALSE;
    BOOLEAN culling = TRUE;
    BOOLEAN geometry_debug = FALSE;
    BOOLEAN environment_mapping = FALSE;
    BOOLEAN bump_mapping = TRUE;
    BOOLEAN enable_light = TRUE;
    BOOLEAN shadow_volume = TRUE;
    BOOLEAN render_z_buffer = TRUE;
    BOOLEAN render_shadow_volume = TRUE;
    BOOLEAN render_shadowed_scene = TRUE;
    BOOLEAN render_ambient = TRUE;

    double FOV = 60.0;
    double near_plane = 1.0;
    double far_plane = 1000.0;
    double aspect = 1.0;
    tScene scene = tScene::master_scene;
    GLuint vertexShaderObject, fragmentShaderObject;
};

// initialize OpenGL state
void initGL()
{
    float specular[] = { 1.0, 1.0, 1.0, 1.0 };
    float shininess[] = { 100.0 };
    float position[] = { 0.0, 10.0, 1.0, 0.0 };	// light source position

    glShadeModel(GL_SMOOTH);   // enable smooth shading
    glClear(GL_DEPTH_BUFFER_BIT);   // clear depth buffer
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);  // black background
    glClearDepth(1.0f);        // depth buffer setup
    glEnable(GL_DEPTH_TEST);   // enables depth testing
    glDepthFunc(GL_LEQUAL);    // configure depth testing@
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);          // really nice perspective calculations

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	// set polygon drawing mode to fill front and back of each polygon
    glShadeModel(GL_SMOOTH);             		// set shading to smooth

    // Generate material properties:
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    // Generate light source:
    glLightfv(GL_LIGHT0, GL_POSITION, position);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);  // Initialize glut
    GLeeInit();             // Initialize GLee

    initGL();  // Initialize OpenGL

	// Setup window
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_ALPHA | GLUT_STENCIL);   // open an OpenGL context with double buffering, RGBA colors, depth buffering and stencil buffer
    glutInitWindowSize(Window::width, Window::height);      // set initial window size
    glutCreateWindow("CSE167 Tower Defense 3000");    	      // open window and set window title
#if (FULL_SCREEN == FALSE)
    glutFullScreen();
#endif

    GL_DEBUG("setup");

    // Install callback functions:
    glutDisplayFunc(Window::displayCallback);
    glutReshapeFunc(Window::reshapeCallback);
    glutIdleFunc(Window::idleCallback);
    glutKeyboardFunc(Keyboard::keyPressCb);
	glutSpecialFunc(Keyboard::specialFuncCb);
    glutMouseFunc(Mouse::mouseCallback);

    // Allocate all textures in one go

    /* GLSL */
    if (GLEE_ARB_vertex_shader && GLEE_ARB_fragment_shader)
        printf("Ready for GLSL\n");
    else
    {
        printf("Not totally ready :( \n");
        exit(1);
    }
    
    Texture::init();

	// Setup camera and world
    Globals::camera = new Camera();
    Globals::world = new World();

    GL_DEBUG("Initialized camera and world!");

    glutMainLoop();
    return 0;
}