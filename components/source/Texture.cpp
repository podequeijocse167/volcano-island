#include "Texture.h"
#include "Utility.h"

#include <stdlib.h>
#include <stdio.h>
#include <GL/glut.h>
#include <math.h>   // include math functions, such as sin, cos, M_PI
#include <iostream> // allow c++ style console printouts
#include <cassert>
#include "SOIL/SOIL.h"

#define CUBE_MAP_PPM FALSE

static float default_texture_array[] = { 0, 0, 1, 0, 1, 1, 0, 1 };

static unsigned char* loadPPM(const char* filename, int& width, int& height);

Texture::Texture() : Texture(tTextureType::TEXTURE_2D) {}

Texture::Texture(tTextureType type) : texture_type(type)
{
    texture_array = default_texture_array;
}

Texture::Texture(tTextureType type, const char *f_name) : texture_type(type)
{
    load_texture(f_name);
}

void Texture::init()
{
    
}

Texture::~Texture()
{
}

void Texture::bind()
{
    switch (texture_type)
    {
    case GL_TEXTURE_2D:
        bind_texture_2d();
        break;
    case GL_TEXTURE_CUBE_MAP:
        bind_texture_cube_map();
        break;
    }
}

void Texture::unbind()
{
    switch (texture_type)
    {
    case GL_TEXTURE_2D:
        unbind_texture_2d();
        break;
    case GL_TEXTURE_CUBE_MAP:
        unbind_texture_cube_map();
        break;
    }
}

void Texture::bind_texture_2d()
{
    glEnable(GL_TEXTURE_2D);   // enable texture mapping
    glBindTexture(GL_TEXTURE_2D, texture);

    // Make sure no bytes are padded:
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    // Select GL_MODULATE to mix texture with polygon color for shading:
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    // Use bilinear interpolation:
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void Texture::unbind_texture_2d()
{
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}

void Texture::bind_texture_cube_map()
{
    glEnable(GL_TEXTURE_CUBE_MAP);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glActiveTexture(GL_TEXTURE0);
}

void Texture::unbind_texture_cube_map()
{
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    glDisable(GL_TEXTURE_CUBE_MAP);
}

void Texture::draw(const Matrix4& C)
{
    bind();
    Group::draw(C);
    unbind();
    GL_DEBUG("Texture::draw");
}

void Texture::loadTexture(const char *f_name)
{
    return loadTexture(f_name, &texture);
}

// load image file into texture object
void Texture::loadTexture(const char *f_name, GLuint *texture_id)
{
    int twidth, theight;   // texture width/height [pixels]
    unsigned char* tdata;  // texture pixel data

    // Load image file
    tdata = loadPPM((char*)f_name, twidth, theight);
    if (tdata == NULL)
    {
        std::cerr << "Error to load texture";
        return;
    }

    glBindTexture(GL_TEXTURE_2D, *texture_id);

    // Generate the texture
    glTexImage2D(GL_TEXTURE_2D, 0, 3, twidth, theight, 0, GL_RGB, GL_UNSIGNED_BYTE, tdata);

    // Set bi-linear filtering for both minification and magnification
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GL_DEBUG("Texture::loadTexture - end");
}

void Texture::load_texture(const char *f_name)
{
    texture = SOIL_load_OGL_texture(f_name, SOIL_LOAD_AUTO,
        SOIL_CREATE_NEW_ID,
        SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
        );

    glBindTexture(GL_TEXTURE_2D, texture);
}

void Texture::make_cube_map(
    const char* front,
    const char* back,
    const char* top,
    const char* bottom,
    const char* left,
    const char* right
    )
{
    // generate a cube-map texture to hold all the sides

    glActiveTexture(GL_TEXTURE0);

    // load each image and copy into a side of the cube-map texture
    texture = SOIL_load_OGL_cubemap(right, left, top, bottom, back, front, SOIL_LOAD_RGB,
        SOIL_CREATE_NEW_ID,
        SOIL_FLAG_MIPMAPS);

    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

    // format cube map texture
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    GL_DEBUG("Texture::make_cube_map");
}


/** Load a ppm file from disk.
@input filename The location of the PPM file.  If the file is not found, an error message
will be printed and this function will return 0
@input width This will be modified to contain the width of the loaded image, or 0 if file not found
@input height This will be modified to contain the height of the loaded image, or 0 if file not found
@return Returns the RGB pixel data as interleaved unsigned chars (R0 G0 B0 R1 G1 B1 R2 G2 B2 .... etc) or 0 if an error ocured
**/
unsigned char* loadPPM(const char* filename, int& width, int& height)
{
    const int BUFSIZE = 128;
    FILE* fp;
    unsigned int read;
    unsigned char* rawData;
    char buf[3][BUFSIZE];
    char* retval_fgets;
    size_t retval_sscanf;

    if ((fp = fopen(filename, "rb")) == NULL)
    {
        std::cerr << "error reading ppm file, could not locate " << filename << std::endl;
        width = 0;
        height = 0;
        return NULL;
    }

    // Read magic number:
    retval_fgets = fgets(buf[0], BUFSIZE, fp);

    // Read width and height:
    do
    {
        retval_fgets = fgets(buf[0], BUFSIZE, fp);
    } while (buf[0][0] == '#');
    retval_sscanf = sscanf(buf[0], "%s %s", buf[1], buf[2]);
    width = atoi(buf[1]);
    height = atoi(buf[2]);

    // Read maxval:
    do
    {
        retval_fgets = fgets(buf[0], BUFSIZE, fp);
    } while (buf[0][0] == '#');

    // Read image data:
    rawData = new unsigned char[width * height * 3];
    read = fread(rawData, width * height * 3, 1, fp);
    fclose(fp);
    if (read != 1)
    {
        std::cerr << "error parsing ppm file, incomplete data" << std::endl;
        delete[] rawData;
        width = 0;
        height = 0;
        return NULL;
    }

    return rawData;
}