#include "Island.h"
#include "Resources.h"
#include "main.h"

Island::Island() : ObjectModel()
{
    has_shadow = FALSE;
    mesh.m_withAdjacencies = FALSE;
    init(island_model);

    texture = new Texture(Texture::tTextureType::TEXTURE_2D, island_texture);
}

Island::~Island()
{
    delete texture;
}

void Island::render()
{
    texture->bind();
    /*glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Globals::world->m_island_diffuse_texture->get_id());
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, Globals::world->m_island_normal_texture->get_id());*/
    ObjectModel::render();
    texture->unbind();
}
