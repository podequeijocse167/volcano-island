#include "LSystem.h"
#include "DataType.h"
#include "Utility.h"

#include <Windows.h>
#include <Mmsystem.h>
#include <GL/glut.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <math.h>
#include <time.h>

using namespace std;

const float PI = 3.14, DEPTH = 4;
// Start and end of camera movement
const int ENDX = 10, STARTX = -500;
// Angle of branches, and depth of tree
float ANGLE = 20;
vector<string> *trees = new vector<string>();

double lastTime = 0, elapsedTime = 0, lastElapsedTime = 0;

bool cam = false;

float eyeX, eyeY, eyeZ, lookX, lookY, lookZ,
upX, upY, upZ, fieldOfView, length = 0.001, num = 0,
incr = 0.1;

float lineWidth = 5;
// L-System
string str = "X";

LSystem::LSystem()
{
    depth = 0;
    srand(time(NULL));
    trees = new vector<string>();
    for (int i = 0; i <= DEPTH; i++)
    {
        expand(num);
    }
    num = (float)rand() / RAND_MAX;
    expand(num);

    GL_DEBUG("LSystem::LSystem");
}

LSystem::~LSystem()
{
}

void push()
{
    glPushMatrix();
    if (lineWidth > 1)
        lineWidth -= 1;
}

void pop()
{
    glPopMatrix();
    lineWidth += 1;
}

void rotL()
{
    glRotatef(ANGLE, 1, 0, 0);
    glRotatef(ANGLE * 4, 0, 1, 0);
    glRotatef(ANGLE, 0, 0, 1);
}

void rotR()
{
    glRotatef(-ANGLE, 1, 0, 0);
    glRotatef(ANGLE * 4, 0, 1, 0);
    glRotatef(-ANGLE, 0, 0, 1);
}

void leaf()
{
    GL_DEBUG("LSystem::leaf - begin");

    glPushAttrib(GL_LIGHTING_BIT); //saves current lighting stuff
    GLfloat ambient[4] = { 0.50, 1.0, 0.0 };    // ambient reflection
    GLfloat specular[4] = { 0.55, 1.0, 0.0 };   // specular reflection
    GLfloat diffuse[4] = { 0.50, 0.9, 0.0 };   // diffuse reflection

    // set the ambient reflection for the object
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
    // set the diffuse reflection for the object
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
    // set the specular reflection for the object      
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
    // set the size of the specular highlights
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 20.0);

    //glutSolidCube(depth+1);
    glBegin(GL_TRIANGLES);
    glVertex3f(0, 0, 0);
    glVertex3f(0.2, 0, 0.3);
    glVertex3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(-0.2, 0, -0.3);
    glVertex3f(0, 1, 0);
    glEnd();
    glPopAttrib();

    GL_DEBUG("LSystem::leaf");
}

void drawLine()
{
    GL_DEBUG("LSystem::drawLine");

    glPushAttrib(GL_LIGHTING_BIT);//saves current lighting stuff

    //glColor3f(0.55, 0.27, 0.07);
    GLfloat ambient[4] = { 0.55, 0.27, 0.07 };    // ambient reflection
    GLfloat specular[4] = { 0.55, 0.27, 0.07 };   // specular reflection
    GLfloat diffuse[4] = { 0.55, 0.27, 0.07 };   // diffuse reflection

    // set the ambient reflection for the object
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
    // set the diffuse reflection for the object
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
    // set the specular reflection for the object      
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
    glLineWidth(lineWidth);

    glBegin(GL_LINES);

    glVertex3f(0, 0, 0);
    glVertex3f(0, length, 0);
    glEnd();

    glTranslatef(0, length, 0);
    glPopAttrib();

    GL_DEBUG("LSystem::drawLine");
}

void LSystem::draw_system()
{
    string ch = "";
    string LSystem = trees->at(depth);

    for (UINT32 i = 0; i < LSystem.length(); i++)
    {
        ch = LSystem.at(i);

        if (ch.compare("D") == 0 || ch.compare("X") == 0)
        {
            drawLine();
        }
        else if (ch.compare("[") == 0)
        {
            push();
        }
        else if (ch.compare("]") == 0)
        {
            pop();
        }
        else if (ch.compare("V") == 0)
        {
            leaf();
        }
        else if (ch.compare("R") == 0)
        {
            rotR();
        }
        else if (ch.compare("L") == 0)
        {
            rotL();
        }
    }
}

void LSystem::expand(float num)
{
    string ch = "";

    for (UINT32 i = 0; i < str.length(); i++)
    {
        ch = str.at(i);

        if (ch.compare("D") == 0)
        {
            str.replace(i, 1, "DD");
            i = i + 1;
        }
        else if (ch.compare("X") == 0)
        {

            if (num < 0.4)
            {
                //LSystem.replace(i, 1, "D[LX]D[RX]LX");
                str.replace(i, 1, "D[LXV]D[RXV]LX");

            }
            else
            {
                //LSystem.replace(i, 1, "D[RX]D[LX]RX");
                str.replace(i, 1, "D[RXV]D[LXV]RX");
            }
            i = i + 13;	//11
        }

    }
    trees->push_back(str);
}

void LSystem::render()
{
    Shader::push();
    GL_DEBUG("LSystem::render - begin");

    glPushAttrib(GL_LIGHTING_BIT); //saves current lighting stuff

    GLfloat ambient[4] = { 0.82, 0.41, 0.12 };    // ambient reflection
    GLfloat diffuse[4] = { 0.82, 0.41, 0.12 };   // diffuse reflection    
    // set the ambient reflection for the object
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
    // set the diffuse reflection for the object
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);

    glBegin(GL_TRIANGLES);
        glVertex3f(-20, 0, -20);
        glVertex3f(20, 0, -20);
        glVertex3f(20, 0, 20);
        glVertex3f(-20, 0, 20);
        glVertex3f(-20, 0, -20);
        glVertex3f(20, 0, 20);
    glEnd();

    glPopAttrib();

    draw_system();

    GL_DEBUG("LSystem::render");
    Shader::pop();
}

void LSystem::update()
{
    if (lastTime == 0)
        lastTime = timeGetTime();

    elapsedTime = timeGetTime() - lastTime;

    // Change the angle to make it blow in the wind
    float numR = (float)rand() / RAND_MAX;


    if (ANGLE > 21.5)
    {
        if (numR < 0.5)
        {
            incr = -0.15;
        }
        else
        {
            incr = -0.1;
        }
    }
    else if (ANGLE < 18.5)
    {
        if (numR > 0.5)
        {
            incr = 0.15;
        }
        else
        {
            incr = 0.1;
        }
    }
    ANGLE += incr;

    if (depth < DEPTH)
        length += 0.00005;

    if (elapsedTime - lastElapsedTime > 2000 && depth < DEPTH)
    {
        depth++;
        lastElapsedTime = elapsedTime;
        cout << "a ";
    }

    elapsedTime = elapsedTime / 5000;
    float t = (sin((elapsedTime*PI - PI / 2)) + 1) / 2;
    float p = (1 - t)*STARTX + t*ENDX;
}