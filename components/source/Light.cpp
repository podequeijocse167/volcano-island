#include "Light.h"
#include "main.h"
#include "Utility.h"

static float default_position[] = { 0.0, 0.0, 0.0, 1.0 };
static float default_direction[] = { 0.0, 0.0, 1.0 };
static Color default_ambient(0.0, 0.0, 0.0, 1.0);
static Color default_diffuse(1.0, 1.0, 1.0, 1.0);
static Color default_specular(1.0, 1.0, 1.0, 1.0);
static float default_spot_cutoff = 180.0;
static float default_spot_exponent = 0.0;

Light::Light() : Light(GL_LIGHT0) {}

Light::Light(tLightID light_ID) : Light(light_ID, default_position) {}

Light::Light(tLightID light_ID, float position[]) : Light(light_ID, position, default_direction) {}

Light::Light(tLightID light_ID, float position[], float direction[]) :
light_ID(light_ID), mask(AMBIENT), m_enable(TRUE), ambient_intensity(0.0f), diffuse_intensity(0.0f), specular_intensity(0.0f)
{
    set_position(position);
    set_direction(direction);
    ambient = default_ambient;
    diffuse = default_diffuse;
    speculer = default_specular;
    spot_cutoff = default_spot_cutoff;
    spot_exponent = default_spot_exponent;
    light_ID = GL_LIGHT0;
}

Light::~Light()
{
    glDisable(light_ID);
}

void Light::set_position(float position[])
{
    for (register UINT8 i = 0; i < 4; ++i)
        this->m_position[i] = position[i];
}

void Light::set_direction(float direction[])
{
    for (register UINT8 i = 0; i < 3; ++i)
        this->m_direction[i] = direction[i];
}

void Light::enable_property(tLightType type)
{
    mask = (tLightType)(mask | type);
}

void Light::set_property(tLightType type, float r, float g, float b, float a)
{
    Light::set_property(type, Color(r, g, b));
}

void Light::set_property(tLightType type, const Color& c)
{
    enable_property(type);
    switch (type)
    {
    case tLightType::AMBIENT:
        ambient = c;
        break;
    case tLightType::DIFFUSE:
        diffuse = c;
        break;
    case tLightType::SPECULAR:
        speculer = c;
        break;
    }
}

void Light::set_property(tLightType type, float p)
{
    enable_property(type);
    switch (type)
    {
    case tLightType::SPOT_CUTOFF:
        spot_cutoff = p;
        break;
    case tLightType::SPOT_EXPONENT:
        spot_exponent = p;
        break;
    }
}

float Light::get_property(tLightType type)
{
    switch (type)
    {
    case tLightType::SPOT_CUTOFF:
        return spot_cutoff;
    case tLightType::SPOT_EXPONENT:
        return spot_exponent;
    }
    return 0.0;
}

float *Light::get_property3f(tLightType type)
{
    switch (type)
    {
    case tLightType::POSITION: return m_position;
    case tLightType::SPOT_DIRECTION: return m_direction;
    }
    return nullptr;
}

float *Light::get_position_eye_space3f()
{
    Matrix4 model_view;
    Vector4 position(this->m_position, 1.0);

    model_view = this->get_model_view();
    position = Globals::camera->getMatrix() * model_view * position;
    return position.get_pointer3f();
}

void Light::get_property(tLightType type, float **value)
{
    switch (type)
    {
    case tLightType::POSITION:
        *value = m_position;
        break;
    case tLightType::SPOT_DIRECTION:
        *value = m_direction;
        break;
    }
}

void Light::unset_property(tLightType type)
{
    mask = (tLightType)(mask & ~(mask & type));
}

BOOLEAN Light::has_property(tLightType type)
{
    return mask & type ? TRUE : FALSE;
}

void Light::draw(const Matrix4& C)
{
    static Matrix4 transformation = C;

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadMatrixd(transformation.transpose().get_pointer());

    render();

    glPopMatrix();
}

Matrix4 Light::get_model_view() const
{
    if (owner == nullptr)
        return Matrix4::matrix_identity;
    else
        return owner->get_model_view();
}

void Light::render()
{
    Matrix4 model_view;

    if (!Globals::enable_light || !m_enable){
        glDisable(light_ID);
        return;
    }

    model_view = Globals::camera->getMatrix() * this->get_model_view();
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glLoadMatrixf(model_view.transpose().get_pointer3f());

    glLightfv(light_ID, GL_POSITION, m_position);

    if (mask & tLightType::AMBIENT)
    {
        glLightfv(light_ID, GL_AMBIENT, (ambient * ambient_intensity).get_pointer());
    }
    if (mask & tLightType::DIFFUSE)
    {
        glLightfv(light_ID, GL_DIFFUSE, (diffuse * diffuse_intensity).get_pointer());
    }
    if (mask & tLightType::SPECULAR)
    {
        glLightfv(light_ID, GL_SPECULAR, (speculer * specular_intensity).get_pointer());
    }
    if (mask & tLightType::SPOTLIGHT)
    {
        glLightf(light_ID, GL_SPOT_EXPONENT, float(spot_exponent));
        glLightf(light_ID, GL_SPOT_CUTOFF, spot_cutoff);
        glLightfv(light_ID, GL_SPOT_DIRECTION, m_direction);
    }

    glPopMatrix();
    glEnable(light_ID);
    GL_DEBUG("Light::render");
}