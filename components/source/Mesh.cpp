#include "Mesh.h"
#include "Common.h"

#include <assert.h>
#include <map>

#define DEBUG_ASSERT TRUE

void Mesh::MeshEntry::init(const std::vector<Vertex>& Vertices,
    const std::vector<unsigned int>& Indices)
{
    NumIndices = Indices.size();

    /* Vertex buffer */
    glGenBuffers(1, &VB);
    glBindBuffer(GL_ARRAY_BUFFER, VB);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)* Vertices.size(), &Vertices[0], GL_STATIC_DRAW);

    /* Index buffer */
    glGenBuffers(1, &IB);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IB);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)* NumIndices, &Indices[0], GL_STATIC_DRAW);
}

Mesh::Mesh() : Mesh(GL_TRIANGLES) {}

Mesh::Mesh(GLuint with_adj) : m_withAdjacencies(with_adj)
{
}

Mesh::~Mesh()
{
    clear();
}

void Mesh::clear()
{
}

bool Mesh::load_mesh(const std::string& file_name)
{
    BOOLEAN result = FAIL;

    const struct aiScene* pScene = aiImportFile(file_name.c_str(), aiProcessPreset_TargetRealtime_MaxQuality);

    if (pScene)
    {
        result = init_from_scene(pScene, file_name);
    }
    else
    {
        printf("Error parsing '%s'\n", file_name.c_str());
    }

    return result;
}

bool Mesh::init_from_scene(const aiScene* pScene, const std::string& Filename)
{
    m_entries.resize(pScene->mNumMeshes);

    // Initialize the meshes in the scene one by one
    for (UINT32 i = 0; i < m_entries.size(); i++)
    {
        const aiMesh* paiMesh = pScene->mMeshes[i];
        init_mesh(i, paiMesh);
    }
    return TRUE;
}

void Mesh::init_mesh(UINT32 Index, const aiMesh* paiMesh)
{
    //m_entries[Index].MaterialIndex = paiMesh->mMaterialIndex;

    std::vector<Vertex> Vertices;
    std::vector<unsigned int> Indices;

    const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

    for (unsigned int i = 0; i < paiMesh->mNumVertices; i++)
    {
        const aiVector3D* pPos = &(paiMesh->mVertices[i]);
        const aiVector3D* pNormal = &(paiMesh->mNormals[i]);
        const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &Zero3D;
        Vertex v(Vector3f(pPos->x, pPos->y, pPos->z),
            Vector2f(pTexCoord->x, pTexCoord->y),
            Vector3f(pNormal->x, pNormal->y, pNormal->z));

        Vertices.push_back(v);
    }

    for (unsigned int i = 0; i < paiMesh->mNumFaces; i++)
    {
        const aiFace& Face = paiMesh->mFaces[i];
        assert(Face.mNumIndices == 3);
        Indices.push_back(Face.mIndices[0]);
        Indices.push_back(Face.mIndices[1]);
        Indices.push_back(Face.mIndices[2]);
    }

    m_entries[Index].init(Vertices, Indices);
}

void Mesh::init_mesh(Vector3f *vertice, Vector2f *texture, Vector3f *normal, UINT32 n_vertices, Face *faces, UINT32 n_faces)
{
    std::vector<Vertex> vertices;
    std::vector<UINT32> indices;
    Vector2f zero(0, 0);
    m_entries.resize(1);

    for (UINT32 i = 0; i < n_vertices; i++)
    {
        UINT32 a = sizeof(Vector3f);
        const Vector3f* pPos = &(vertice[i]);
        const Vector2f* pTex = (texture ? &(texture[i]) : &zero);
        const Vector3f* pNormal = &(normal[i]);
        Vertex v(Vector3f(pPos->x, pPos->y, pPos->z),
            Vector2f(pTex->x, pTex->y),
            Vector3f(pNormal->x, pNormal->y, pNormal->z));

        vertices.push_back(v);
    }

    if (m_withAdjacencies == TRUE)
    {
        find_adjacencies(vertice, faces, n_faces, indices);
    } else {
        for (UINT32 i = 0; i < n_faces; i++)
        {
            const Face& face = faces[i];
            indices.push_back(face.index[0]);
            indices.push_back(face.index[1]);
            indices.push_back(face.index[2]);
        }
    }
    m_entries[0].init(vertices, indices);
}

void Mesh::find_adjacencies(Vector3f *vertices, const Face* faces, UINT32 n_faces, std::vector<UINT32>& Indices)
{
    // Step 1 - find the two triangles that share every edge
    for (UINT32 i = 0; i < n_faces; i++)
    {
        const Face& face = faces[i];

        Face unique;

        // If a position vector is duplicated in the VB we fetch the 
        // index of the first occurrence.
        for (UINT32 j = 0; j < 3; j++)
        {
            UINT32 index = face.index[j];
            Vector3f& v = vertices[index];

            if (m_posMap.find(v) == m_posMap.end())
            {
                m_posMap[v] = index;
            }
            else
            {
                index = m_posMap[v];
            }
            
            unique.index[j] = index;
        }

        m_uniqueFaces.push_back(unique);

        Edge e1(unique.index[0], unique.index[1]);
        Edge e2(unique.index[1], unique.index[2]);
        Edge e3(unique.index[2], unique.index[0]);

        m_indexMap[e1].add_neighbor(i);
        m_indexMap[e2].add_neighbor(i);
        m_indexMap[e3].add_neighbor(i);
    }

    // Step 2 - build the index buffer with the adjacency info
    for (UINT32 i = 0; i < n_faces; i++)
    {
        const Face& face = m_uniqueFaces[i];

        for (UINT32 j = 0; j < 3; j++)
        {
            Edge e(face.index[j], face.index[(j + 1) % 3]);
            Neighbors n = m_indexMap[e];
            UINT32 OtherTri = n.get_other(i);
            
#if(DEBUG_ASSERT == TRUE)
            assert(m_indexMap.find(e) != m_indexMap.end());
            assert(OtherTri != -1);
#endif
            const Face& OtherFace = m_uniqueFaces[OtherTri];
            UINT32 OppositeIndex = OtherFace.get_opposite_index(e);

            Indices.push_back(face.index[j]);
            Indices.push_back(OppositeIndex);
        }
    }
}

void Mesh::render()
{
    GLuint geometry = m_withAdjacencies ? GL_TRIANGLES_ADJACENCY_ARB : GL_TRIANGLES;

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, m_entries[0].VB);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLuint*)0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)12);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)20);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_entries[0].IB);

    glDrawElements(geometry, m_entries[0].NumIndices, GL_UNSIGNED_INT, 0);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}