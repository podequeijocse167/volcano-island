/*
Copyright (C) 2006 So Yamaoka

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/

#include "Shader.h"
#include "Utility.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <iostream>

BOOLEAN Shader::block = FALSE;
std::stack<GLhandleARB> Shader::shader_stack;

Shader::Shader(const char *vert, const char *frag) : Shader(vert, frag, "") {}

Shader::Shader(const char *vert, const char *frag, const char *geo) : 
vert_name(vert), frag_name(frag), geo_name(geo), n_uniform_location(0), m_enable(TRUE)
{
    char *shader;

    init();

    if (vert_name.length())
    {
        shader = read(vert);

        setup(VERTEX_SHADER, shader);

        delete[] shader;
    }
    if (geo_name.length())
    {
        shader = read(geo);

        setup(GEOMETRY_SHADER, shader);

        delete[] shader;
    }
    if (frag_name.length())
    {
        shader = read(frag);

        setup(FRAGMENT_SHADER, shader);

        delete[] shader;
    }

    finalize();
    printLog();
}

Shader::~Shader()
{
    glDeleteObjectARB(m_shader_prog);
}

GLint& Shader::operator[](std::string name_uniform_location)
{
#if(DEBUG == TRUE)
    if (m_uniform_location_map.find(name_uniform_location) == m_uniform_location_map.end())
        fprintf(stderr, "Uniform location no registered\n");
#endif
    return m_uniform_location_map[name_uniform_location];
}

void Shader::bind()
{
    if (block || m_enable == FALSE)
        return;
    glUseProgramObjectARB(m_shader_prog);
    shader_stack.push(m_shader_prog);
}

void Shader::unbind()
{
    if (block || m_enable == FALSE)
        return;
	glUseProgramObjectARB(0);
    if (shader_stack.size() != 0)
    {
        assert(shader_stack.top() == m_shader_prog);
        shader_stack.pop();

        if (shader_stack.size() != 0)
            glUseProgramObjectARB(shader_stack.top());
    }
}

void Shader::push()
{
    glUseProgramObjectARB(0);
}

void Shader::pop()
{
    if (shader_stack.size() != 0)
        glUseProgramObjectARB(shader_stack.top());
}

GLhandleARB Shader::getPid()
{
    return m_shader_prog;
}

GLint Shader::add_uniform_location(std::string name)
{
    bind();
    GLint uniform_location = glGetUniformLocation(m_shader_prog, name.c_str());
    unbind();

    GL_DEBUG("Shader::add_uniform_location");
    return m_uniform_location_map[name] = uniform_location;
}

void Shader::set_uniform_vec3f(std::string name, GLsizei count, const GLfloat* value)
{
    if (m_enable)
        glUniform3fv(m_uniform_location_map[name], count, value);
}

void Shader::set_uniform_mat3f(std::string name, GLsizei count, BOOLEAN transpose, const GLfloat *value)
{
    if (m_enable)
        glUniformMatrix4fv(m_uniform_location_map[name], 1, transpose, value);
}

void Shader::set_uniform1i(std::string name, GLint value)
{
    if (m_enable)
        glUniform1i(m_uniform_location_map[name], value);
}

void Shader::draw(const Matrix4& C)
{
    bind();
    GL_DEBUG("Shader::draw");

    Group::draw(C);

    unbind();
}

void Shader::printLog(const char* tag)
{
	char glslLog[1024];
	GLsizei glslLogSize;
	glGetInfoLogARB(m_shader_prog, 1024, &glslLogSize, glslLog);
	if(glslLogSize>0)
	{
		fprintf(stderr, "%s:shader error log: %s\n", tag, glslLog);
	}
	else
	{
		fprintf(stdout, "%s:shader all ok.\n", tag);
	}
}

char* Shader::read(const char *filename)
{
	char* shaderFile = 0;
    FILE* fp = new FILE;

    fopen_s(&fp, filename, "rb");
	if(!fp){fprintf(stderr, "File doesn't exist (%s)\n", filename); std::exit(-1);}

	// obtain the file size
	fseek(fp, 0, SEEK_END);
	long size = ftell(fp);
	rewind(fp);

	// alloc memory - will be deleted while setting the shader up
	shaderFile = new char[size+1];

	// copy the file to the shaderFile
	fread(shaderFile, sizeof(char), size, fp);
	shaderFile[size]='\0'; // eliminate the garbage at EOF
	fclose(fp);

	fprintf(stdout, "shaderfile = %s\n", filename);
	return shaderFile;
}

void Shader::init()
{
    m_shader_prog = glCreateProgramObjectARB();
}

void Shader::add_shader(tShaderType type, const char *file)
{
    char* shader_string = read(file);

    setup(type, shader_string);
}

void Shader::setup(tShaderType type, const char *shader)
{
    char glslLog[1024];
    GLsizei glslLogSize;
    GLhandleARB id = glCreateShaderObjectARB(type);

    GL_DEBUG("Shader::setup - end");
	
	glShaderSourceARB(id, 1, &shader, 0);
	glCompileShaderARB(id);
	glGetInfoLogARB(id, 1024, &glslLogSize, glslLog);
    if (glslLogSize)
    {
        printf("%s program log: %s\n", shader_type2string(type), glslLog);
#if(DEBUG == TRUE)
        system("pause");
#endif
    }
	glAttachObjectARB(m_shader_prog, id);

    // delete shader objects as soon as they have been attached to a program
    glDeleteObjectARB(id);
    GL_DEBUG("Shader::setup - end");
}

BOOLEAN Shader::finalize()
{
    GLint Success = 0;
    GLchar ErrorLog[1024] = { 0 };

    glLinkProgramARB(m_shader_prog);

    glGetProgramiv(m_shader_prog, GL_LINK_STATUS, &Success);
    if (Success == 0)
    {
        glGetProgramInfoLog(m_shader_prog, sizeof(ErrorLog), NULL, ErrorLog);
        fprintf(stderr, "Error linking shader program: '%s'\n", ErrorLog);
#if(DEBUG == TRUE)
        system("pause");
#endif
        return FALSE;
    }

    glValidateProgram(m_shader_prog);
    glGetProgramiv(m_shader_prog, GL_VALIDATE_STATUS, &Success);
    if (!Success)
    {
        glGetProgramInfoLog(m_shader_prog, sizeof(ErrorLog), NULL, ErrorLog);
        fprintf(stderr, "Invalid shader program: '%s'\n", ErrorLog);
#if(DEBUG == TRUE)
        system("pause");
#endif
        return FALSE;
    }
    GL_DEBUG("Shader::finalize");
    return TRUE;
}

char* Shader::shader_type2string(tShaderType type)
{
    switch (type)
    {
    case FRAGMENT_SHADER: return "fragment";
    case VERTEX_SHADER: return "vertex";
    case GEOMETRY_SHADER: return "geometry";
    default: return "unkown";
    }
}