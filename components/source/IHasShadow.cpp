#include "IHasShadow.h"
#include "Utility.h"

std::list<IHasShadow*> IHasShadow::list;

IHasShadow::IHasShadow()
{
    list.push_back(this);
}


IHasShadow::~IHasShadow()
{
    list.remove(this);
}
