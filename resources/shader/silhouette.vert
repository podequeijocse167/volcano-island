#version 330 compatibility

layout (location = 0) in vec3 Position; 
layout (location = 1) in vec2 TexCoord; 
layout (location = 2) in vec3 Normal; 

out vec3 WorldPos0; 

void main() 
{ 
    vec4 PosL = vec4(Position, 1.0);
    WorldPos0 = (gl_ModelViewMatrix * PosL).xyz; 
    gl_Position = gl_ModelViewProjectionMatrix * PosL;
}