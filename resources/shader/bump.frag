#version 120

uniform sampler2D diffuseTexture;
uniform sampler2D normalTexture;
	
// New bumpmapping
varying vec3 lightVec;
varying vec3 halfVec;
varying vec3 eyeVec;

 void main()
 { 
	vec4 diffTex = texture2D(diffuseTexture, gl_TexCoord[0].st);
	vec4 normTex = texture2D(normalTexture, gl_TexCoord[0].st);

	// lookup normal from normal map, move from [0,1] to  [-1, 1] range, normalize
	//vec3 normal = normTex.rgb;// - 1.0;
	vec3 normal = 2.0 * texture2D (normalTexture, gl_TexCoord[0].st).rgb - 1.0;
	
	normal = -normalize (normal);

	//gl_FragColor = normTex;//0.5 * normTex + 0.5 * diffTex;
	//gl_FragColor = vec4(normal, 1.0);
	//vec3 lightVec = vec3(0.5, 0.5, 0.5);
	//gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);		
	// compute diffuse lighting

	float lamberFactor = max (dot (lightVec, normal), 0.0) ;

	vec4 diffuseMaterial;// = vec4(0.5, 0.5, 0.5, 0.0);
	vec4 diffuseLight;//  = vec4(0.5, 0.5, 0.5, 0.0);
	
	// compute specular lighting
	vec4 specularMaterial ;
	vec4 specularLight ;
	float shininess ;
  
	// compute ambient
	vec4 ambientLight = gl_LightSource[0].ambient;	
	
	if (lamberFactor > 0.0)
	{
		diffuseMaterial = texture2D (diffuseTexture, gl_TexCoord[0].st);
		diffuseLight  = gl_LightSource[0].diffuse;
		
		// In doom3, specular value comes from a texture 
		specularMaterial =  vec4(0.8);
		specularLight = gl_LightSource[0].specular;

		//vec3 dnorm = vec3(-normal.x, normal.y, -normal.z);

		shininess = 0;//pow (max (dot (halfVec, normal), 0.0), 2.0)  ;
		 
		gl_FragColor =	diffuseMaterial * lamberFactor;// diffuseLight * lamberFactor ;
		gl_FragColor +=	specularMaterial * specularLight * shininess ;	
		

		//gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);			
	
	}
	
	gl_FragColor +=	ambientLight;
	//else {gl_FragColor = texture2D(diffuseTexture, gl_TexCoord[0].st);}
	//gl_FragColor += 0.5
	gl_FragColor = texture2D(diffuseTexture, gl_TexCoord[1].st);
 }