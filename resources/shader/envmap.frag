#version 330 compatibility

in vec3 pos_eye;
in vec3 n_eye;
uniform samplerCube cubeTex;

uniform sampler2D tex;

//uniform mat4 V; // view matrix
//out vec4 frag_colour;

//varying vec3 normal;

void main () {
    /* reflect ray around normal from eye to surface */
    vec3 incident_eye = normalize (pos_eye);
    vec3 normal_eye = normalize (n_eye);

    vec3 reflected = reflect (incident_eye, normal_eye);
    // convert from eye to world space

    //reflected = vec3 (inverse (gl_ModelViewMatrix) * vec4 (reflected, 0.0));


    reflected = - vec3 (inverse (gl_ModelViewMatrix) * vec4 (reflected, 0.0));

    reflected = vec3(-reflected.x, reflected.y, reflected.z);


    float ratio = 1.0 /1.3333;
    vec3 refracted = refract (incident_eye, normal_eye, ratio);
    refracted = -vec3 (inverse (gl_ModelViewMatrix) * vec4 (refracted, 0.0));

    //refracted = - vec3 (inverse (gl_ModelViewMatrix) * vec4 (refracted, 0.0));

    refracted = vec3(refracted.x, refracted.y, refracted.z);


    //vec3 total = (reflected + refracted)/2;

    //gl_FragColor = vec4( vec3(texture(cubeTex, reflected)), 1.0);
    vec4 c1 = vec4( vec3(texture(cubeTex, reflected)), 1.0);
    vec4 c2 = vec4( vec3(texture(cubeTex, refracted)), 1.0);

    //gl_FragColor = (c1 + c2)/1.9;//vec4( vec3(texture(cubeTex, refracted)), 1.0);


    gl_FragColor = vec4( vec3(texture(cubeTex, reflected)), 0.6);
    //gl_FragColor = 0.5*vec4( vec3(texture(cubeTex, reflected)), 0.7) + 0.5*vec4(vec3(texture(tex, gl_TexCoord[0].st)), 0.7);
    //gl_FragColor = vec4( vec3(texture(tex, gl_TexCoord[0].st)), 0.7);

}