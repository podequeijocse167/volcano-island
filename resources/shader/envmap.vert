#version 330 compatibility

//in vec3 vp; // positions from mesh
//in vec3 vn; // normals from mesh
//uniform mat4 P, V, M; // proj, view, model matrices

out vec3 pos_eye;
out vec3 n_eye;

//varying vec3 normal;

void main () {


    //normal = normalize( vec3( gl_ModelViewProjectionMatrix * vec4(gl_Normal, 0.0) ) );
    // COOL EFFECT
    //normal = normalize(gl_Normal);// vec3(gl_Normal.x, 0.1, gl_Normal.z));

    pos_eye = vec3(gl_ModelViewMatrix * gl_Vertex);
    n_eye = vec3 (gl_ModelViewMatrix * vec4(gl_Normal, 0.0));

    //pos_eye = vec3 (V * M * gl_Vertex);
    //n_eye = vec3 (V * M * vec4(gl_Normal, 0.0));

    //pos_eye = vec3 (V * M * vec4(vp, 1.0));
    //n_eye = vec3 (V * M * vec4(vn, 0.0));

    //gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

    gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;

    //gl_Position = P * V * M * gl_Vertex;

}